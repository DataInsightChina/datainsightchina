# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/user/Desktop/DataAnalysisChina/src/Cython/src/c_multiply.c" "/home/user/Desktop/DataAnalysisChina/src/Cython/build/CMakeFiles/CythonMultiply.dir/src/c_multiply.c.o"
  "/home/user/Desktop/DataAnalysisChina/src/Cython/src/multiply.c" "/home/user/Desktop/DataAnalysisChina/src/Cython/build/CMakeFiles/CythonMultiply.dir/src/multiply.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/user/anaconda3/include/python3.5m"
  "/home/user/anaconda3/lib/python3.5/site-packages/numpy/core/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
